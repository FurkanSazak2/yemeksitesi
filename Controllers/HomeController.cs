﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using YemekSitesi.DAL;
using YemekSitesi.Models;

namespace YemekSitesi.Controllers
{
    public class HomeController : Controller
    {
        private YemekContext db = new YemekContext();

        // GET: /Home
        public ActionResult Index()
        {
            return View(db.Malzemeler.ToList());
        }

        [HttpPost]
        public string Index(string textarea1)
        {
            string[] malzemeler = Regex.Split(textarea1, "\r\n");

            LinkedList<int> malzemeIDs = new LinkedList<int>();
            var allmalzeme = db.Malzemeler.ToList();

            for(int i=0;i<malzemeler.Length;++i)
            {
                   item.Isim.Equals(malzemeler);
            }
            


          
            var yemekler = db.Database.SqlQuery<Yemek>(
                        "SELECT y1.ID, y1.Isim, y1.Hazirlanis, y1.Kisi, y1.Mensei, y1.PisirmeSuresi, y1.Tur, y1.Zorluk " +
                        "FROM ICERIK t1 " +
                        "JOIN ICERIK t2 ON t1.YemekID = t2.YemekID AND t2.MalzemeID = @p0 " +
                        "JOIN ICERIK t3 ON t2.YemekID = t3.YemekID AND t3.MalzemeID = @p1 " +
                        "JOIN YEMEK y1 ON y1.ID = t3.YemekID " +
                        "WHERE t1.MalzemeID = @p2", 1, 2, 3);


            //return yemekler.ToList<Yemek>()[0].Isim;
            return allmalzeme[0].Isim;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }

  class item
  {
    public static object Isim { get; internal set; }
  }
}