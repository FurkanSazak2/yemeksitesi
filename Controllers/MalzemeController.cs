﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YemekSitesi.Models;
using YemekSitesi.DAL;

namespace YemekSitesi.Controllers
{
    public class MalzemeController : Controller
    {
        private YemekContext db = new YemekContext();

        // GET: /Malzeme/
        public ActionResult Index()
        {
            return View(db.Malzemeler.ToList());
        }

        // GET: /Malzeme/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Malzeme malzeme = db.Malzemeler.Find(id);
            if (malzeme == null)
            {
                return HttpNotFound();
            }
            return View(malzeme);
        }

        // GET: /Malzeme/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Malzeme/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Isim")] Malzeme malzeme)
        {
            if (ModelState.IsValid)
            {
                db.Malzemeler.Add(malzeme);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(malzeme);
        }

        // GET: /Malzeme/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Malzeme malzeme = db.Malzemeler.Find(id);
            if (malzeme == null)
            {
                return HttpNotFound();
            }
            return View(malzeme);
        }

        // POST: /Malzeme/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Isim")] Malzeme malzeme)
        {
            if (ModelState.IsValid)
            {
                db.Entry(malzeme).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(malzeme);
        }

        // GET: /Malzeme/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Malzeme malzeme = db.Malzemeler.Find(id);
            if (malzeme == null)
            {
                return HttpNotFound();
            }
            return View(malzeme);
        }

        // POST: /Malzeme/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Malzeme malzeme = db.Malzemeler.Find(id);
            db.Malzemeler.Remove(malzeme);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
