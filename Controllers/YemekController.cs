﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using YemekSitesi.Models;
using YemekSitesi.DAL;
using PagedList;

namespace YemekSitesi.Controllers
{
    public class YemekController : Controller
    {
        private YemekContext db = new YemekContext();

        // GET: /Yemek/
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.PisirmeSortParm = sortOrder == "Pisirme" ? "pisirme_desc" : "Pisirme";
            var yemekler = from s in db.Yemekler
                           select s;

            if(searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            if(!String.IsNullOrEmpty(searchString))
            {
                yemekler = yemekler.Where(y => y.Isim.ToUpper().Contains(searchString.ToUpper()));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    yemekler = yemekler.OrderByDescending(s => s.Isim);
                    break;
                case "Pisirme":
                    yemekler = yemekler.OrderBy(s => s.PisirmeSuresi);
                    break;
                case "pisirme_desc":
                    yemekler = yemekler.OrderByDescending(s => s.PisirmeSuresi);
                    break;
                default:
                    yemekler = yemekler.OrderBy(s => s.Isim);
                    break;
            }

            int pageSize = 3;
            int pageNumber = (page ?? 1);

            return View(yemekler.ToPagedList(pageNumber,pageSize));
        }

        // GET: /Yemek/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yemek yemek = db.Yemekler.Find(id);
            if (yemek == null)
            {
                return HttpNotFound();
            }
            return View(yemek);
        }

        // GET: /Yemek/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Yemek/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Isim,Mensei,Zorluk,Kisi,PisirmeSuresi,Tur,Hazirlanis")] Yemek yemek)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Yemekler.Add(yemek);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException e)
            {
                ModelState.AddModelError("", "Unable to save changes. Error message:" + e.Message);
            }
            return View(yemek);
        }

        // GET: /Yemek/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yemek yemek = db.Yemekler.Find(id);
            if (yemek == null)
            {
                return HttpNotFound();
            }
            return View(yemek);
        }

        // POST: /Yemek/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Isim,Mensei,Zorluk,Kisi,PisirmeSuresi,Hazirlanis")] Yemek yemek)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(yemek).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException e)
            {
                ModelState.AddModelError("", "Unable to save changes. Error message:" + e.Message);
            }
            return View(yemek);
        }

        // GET: /Yemek/Delete/5
        public ActionResult Delete(int? id, bool? saveChangesError=false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Delete failed. Try again, and if the problem persists see your system administrator.";
            }
            Yemek yemek = db.Yemekler.Find(id);
            if (yemek == null)
            {
                return HttpNotFound();
            }
            return View(yemek);
        }

        // POST: /Yemek/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                Yemek yemek = db.Yemekler.Find(id);
                db.Yemekler.Remove(yemek);
                db.SaveChanges();
            }
            catch (DataException )
            {
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
