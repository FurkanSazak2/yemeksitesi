﻿using System;
using YemekSitesi.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace YemekSitesi.DAL
{
    public class YemekContext : DbContext
    {
        public YemekContext()
            : base("YemekContext")
        {
        }

        public DbSet<Yemek> Yemekler { get; set; }
        public DbSet<Malzeme> Malzemeler { get; set; }
        public DbSet<Icerik> Icerikler { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Icerik>()
            //   .HasRequired(t => t.Yemek)
            //   .WithMany(t => t.Icerikler)
            //   .HasForeignKey(d => d.YemekID)
            //   .WillCascadeOnDelete(true);
            //base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}