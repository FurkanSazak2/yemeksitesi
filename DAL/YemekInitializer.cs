﻿using YemekSitesi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YemekSitesi.DAL
{
    public class YemekInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<YemekContext>
    {
        protected override void Seed(YemekContext context)
        {

            string pilavNasilYapilir = "Öncelikle pilav tenceresine tereyağ " +
                                        "döküp eritilir. Daha sonra tel şehriye katıp kırmızı oluncaya " +
                                        "kadar kavrulur. Sonra suda beklemiş pirinç dökülüp 2 dakika " +
                                        "kavrulur. Üzerine gerekli miktarda su döküldükten sonra " +
                                        "tuz ilave edilip kısık ateşte pişirmeye bırakılır.";
            string mercimekNasilYapilir = "Düdüklü tencereye sıvı yağ dökülür, üzerine soğan doğranıp kavrulur. Daha sonra "+
                                            "salça katılır ve karıştırılır. Üzerine mercimek ve su ilave edilir. Gerekli baharatlar "+
                                            "katıldıktan sonra düdüklü kapağı kapatılır. Düt sesi gelinceye kadar yüksek ateşte daha sonra  "+
                                            "kısık ateşte yeterince süre pişirilir.";
            string pudingNasilYapilir = "Paketin hepsi tencereye boşaltılır. Yeteri miktarda süt katılıp kaynayınca kadar pişirilir. "+
                                        "Kaynadıktan sonra 2 dk kısık ateşte bekletilir. Daha sonra küçük kaplara konup buzdolabında soğutulur.";


            var yemekler = new List<Yemek>
            {
                new Yemek{Isim="Pilav",Kisi=4,PisirmeSuresi=20,Zorluk=3,Hazirlanis=pilavNasilYapilir,Tur=YemekTuru.PILAVLAR},
                new Yemek{Isim="Mercimek Çorbası",Kisi=4,PisirmeSuresi=40,Zorluk=2,Hazirlanis=mercimekNasilYapilir,Tur=YemekTuru.CORBALAR},
                new Yemek{Isim="Puding",Kisi=4,PisirmeSuresi=10,Zorluk=1,Hazirlanis=pudingNasilYapilir,Tur=YemekTuru.TATLILAR},
                new Yemek{Isim="İmam Bayıldı",Kisi=4,PisirmeSuresi=30,Zorluk=5,Tur=YemekTuru.SEBZELI_YEMEKLER},
                new Yemek{Isim="Omlet",Kisi=2,PisirmeSuresi=5,Zorluk=1,Tur=YemekTuru.KAHVALTILIKLAR},
            };

            yemekler.ForEach(y => context.Yemekler.Add(y));
            context.SaveChanges();

            var malzemeler = new List<Malzeme>
            {
                new Malzeme{Isim="Pirinç"},
                new Malzeme{Isim="Tel Şehriye"},
                new Malzeme{Isim="Tereyağ"},
                new Malzeme{Isim="Tuz"},
                new Malzeme{Isim="Su"},
                new Malzeme{Isim="Turuncu Mercimek"},
                new Malzeme{Isim="Salça"},
                new Malzeme{Isim="Sıvı Yağ"},
                new Malzeme{Isim="Kırmızı Biber"},
                new Malzeme{Isim="Hazır Puding"},
                new Malzeme{Isim="Patlıcan"},
                new Malzeme{Isim="Kıyma"},
                new Malzeme{Isim="Yeşil Sivri Biber"},
                new Malzeme{Isim="Çarliston Biber"},
                new Malzeme{Isim="Yumurta"},
                new Malzeme{Isim="Sucuk"},
            };

            malzemeler.ForEach(m => context.Malzemeler.Add(m));
            context.SaveChanges();

            var icerikler = new List<Icerik>
            {
                new Icerik{YemekID=1,MalzemeID=1,MiktarAdeti=2,MiktarTuru=MiktarTuru.SU_BARDAGI},
                new Icerik{YemekID=1,MalzemeID=2,MiktarAdeti=1,MiktarTuru=MiktarTuru.CAY_BARDAGI},
                new Icerik{YemekID=1,MalzemeID=3,MiktarAdeti=1,MiktarTuru=MiktarTuru.YEMEK_KASIGI},
                new Icerik{YemekID=1,MalzemeID=4,MiktarAdeti=2,MiktarTuru=MiktarTuru.CAY_KASIGI},
                new Icerik{YemekID=1,MalzemeID=5,MiktarAdeti=4,MiktarTuru=MiktarTuru.SU_BARDAGI},
                new Icerik{YemekID=2,MalzemeID=6,MiktarAdeti=2,MiktarTuru=MiktarTuru.SU_BARDAGI},
                new Icerik{YemekID=2,MalzemeID=7,MiktarAdeti=1,MiktarTuru=MiktarTuru.YEMEK_KASIGI},
                new Icerik{YemekID=2,MalzemeID=8,MiktarAdeti=1,MiktarTuru=MiktarTuru.YEMEK_KASIGI},
                new Icerik{YemekID=2,MalzemeID=9,MiktarAdeti=2,MiktarTuru=MiktarTuru.CAY_KASIGI},
                new Icerik{YemekID=3,MalzemeID=10,MiktarAdeti=1,MiktarTuru=MiktarTuru.ADET},
                new Icerik{YemekID=4,MalzemeID=11,MiktarAdeti=4,MiktarTuru=MiktarTuru.ADET},
                new Icerik{YemekID=4,MalzemeID=12,MiktarAdeti=250,MiktarTuru=MiktarTuru.GRAM},
                new Icerik{YemekID=5,MalzemeID=15,MiktarAdeti=2,MiktarTuru=MiktarTuru.ADET}
            };

            icerikler.ForEach(i => context.Icerikler.Add(i));
            context.SaveChanges();

        }
    }
}