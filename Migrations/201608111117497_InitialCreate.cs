namespace YemekSitesi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Icerik",
                c => new
                    {
                        IcerikID = c.Int(nullable: false, identity: true),
                        YemekID = c.Int(nullable: false),
                        MalzemeID = c.Int(nullable: false),
                        MiktarAdeti = c.Int(nullable: false),
                        MiktarTuru = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IcerikID)
                .ForeignKey("dbo.Malzeme", t => t.MalzemeID, cascadeDelete: true)
                .ForeignKey("dbo.Yemek", t => t.YemekID, cascadeDelete: true)
                .Index(t => t.YemekID)
                .Index(t => t.MalzemeID);
            
            CreateTable(
                "dbo.Malzeme",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Isim = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Yemek",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Isim = c.String(),
                        Mensei = c.String(),
                        Zorluk = c.Int(nullable: false),
                        Kisi = c.Int(nullable: false),
                        PisirmeSuresi = c.Int(nullable: false),
                        Tur_Isim = c.String(),
                        Hazirlanis = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Icerik", "YemekID", "dbo.Yemek");
            DropForeignKey("dbo.Icerik", "MalzemeID", "dbo.Malzeme");
            DropIndex("dbo.Icerik", new[] { "MalzemeID" });
            DropIndex("dbo.Icerik", new[] { "YemekID" });
            DropTable("dbo.Yemek");
            DropTable("dbo.Malzeme");
            DropTable("dbo.Icerik");
        }
    }
}
