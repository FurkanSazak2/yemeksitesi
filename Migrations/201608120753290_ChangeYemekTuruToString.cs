namespace YemekSitesi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeYemekTuruToString : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Yemek", "Tur", c => c.String());
            DropColumn("dbo.Yemek", "Tur_Isim");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Yemek", "Tur_Isim", c => c.String());
            DropColumn("dbo.Yemek", "Tur");
        }
    }
}
