// <auto-generated />
namespace YemekSitesi.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class requiredForYemek : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(requiredForYemek));
        
        string IMigrationMetadata.Id
        {
            get { return "201608120839563_requiredForYemek"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
