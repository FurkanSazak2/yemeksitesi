namespace YemekSitesi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class requiredForYemek : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Yemek", "Isim", c => c.String(nullable: false));
            AlterColumn("dbo.Yemek", "Hazirlanis", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Yemek", "Hazirlanis", c => c.String());
            AlterColumn("dbo.Yemek", "Isim", c => c.String());
        }
    }
}
