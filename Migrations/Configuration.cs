namespace YemekSitesi.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using YemekSitesi.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<YemekSitesi.DAL.YemekContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "YemekSitesi.DAL.YemekContext";
        }

        protected override void Seed(YemekSitesi.DAL.YemekContext context)
        {
            string pilavNasilYapilir = "�ncelikle pilav tenceresine tereya� " +
                                        "d�k�p eritilir. Daha sonra tel �ehriye kat�p k�rm�z� oluncaya " +
                                        "kadar kavrulur. Sonra suda beklemi� pirin� d�k�l�p 2 dakika " +
                                        "kavrulur. �zerine gerekli miktarda su d�k�ld�kten sonra " +
                                        "tuz ilave edilip k�s�k ate�te pi�irmeye b�rak�l�r.";
            string mercimekNasilYapilir = "D�d�kl� tencereye s�v� ya� d�k�l�r, �zerine so�an do�ran�p kavrulur. Daha sonra " +
                                            "sal�a kat�l�r ve kar��t�r�l�r. �zerine mercimek ve su ilave edilir. Gerekli baharatlar " +
                                            "kat�ld�ktan sonra d�d�kl� kapa�� kapat�l�r. D�t sesi gelinceye kadar y�ksek ate�te daha sonra  " +
                                            "k�s�k ate�te yeterince s�re pi�irilir.";
            string pudingNasilYapilir = "Paketin hepsi tencereye bo�alt�l�r. Yeteri miktarda s�t kat�l�p kaynay�nca kadar pi�irilir. " +
                                        "Kaynad�ktan sonra 2 dk k�s�k ate�te bekletilir. Daha sonra k���k kaplara konup buzdolab�nda so�utulur.";
            string imamBayildiYapilir = "test";
            string omletNasilYapilir = "test2";

            context.Yemekler.AddOrUpdate(
                 y => y.Hazirlanis,
                new Yemek { Isim = "Pilav", Kisi = 4, PisirmeSuresi = 20, Zorluk = 3, Hazirlanis = pilavNasilYapilir, Tur = YemekTuru.PILAVLAR },
                new Yemek { Isim = "Mercimek �orbas�", Kisi = 4, PisirmeSuresi = 40, Zorluk = 2, Hazirlanis = mercimekNasilYapilir, Tur = YemekTuru.CORBALAR },
                new Yemek { Isim = "Puding", Kisi = 4, PisirmeSuresi = 10, Zorluk = 1, Hazirlanis = pudingNasilYapilir, Tur = YemekTuru.TATLILAR },
                new Yemek { Isim = "�mam Bay�ld�", Kisi = 4, PisirmeSuresi = 30, Zorluk = 5, Hazirlanis = imamBayildiYapilir, Tur = YemekTuru.SEBZELI_YEMEKLER },
                new Yemek { Isim = "Omlet", Kisi = 2, PisirmeSuresi = 5, Zorluk = 1, Hazirlanis = omletNasilYapilir, Tur = YemekTuru.KAHVALTILIKLAR }
                );
            context.SaveChanges();

            context.Malzemeler.AddOrUpdate(
                m => m.Isim,
                new Malzeme { Isim = "Pirin�", Tur = MalzemeTuru.BAKLAGIL },
                new Malzeme { Isim = "Tel �ehriye", Tur = MalzemeTuru.BAKLAGIL },
                new Malzeme { Isim = "Tereya�", Tur = MalzemeTuru.DIGER },
                new Malzeme { Isim = "Tuz", Tur = MalzemeTuru.BAHARAT },
                new Malzeme { Isim = "Su", Tur = MalzemeTuru.DIGER },
                new Malzeme { Isim = "Turuncu Mercimek", Tur = MalzemeTuru.BAKLAGIL },
                new Malzeme { Isim = "Sal�a", Tur = MalzemeTuru.DIGER },
                new Malzeme { Isim = "S�v� Ya�", Tur = MalzemeTuru.DIGER },
                new Malzeme { Isim = "K�rm�z� Biber", Tur = MalzemeTuru.BAHARAT },
                new Malzeme { Isim = "Haz�r Puding", Tur = MalzemeTuru.DIGER },
                new Malzeme { Isim = "Patl�can", Tur = MalzemeTuru.SEBZE },
                new Malzeme { Isim = "K�yma", Tur = MalzemeTuru.KIRMIZI_ET },
                new Malzeme { Isim = "Ye�il Sivri Biber", Tur = MalzemeTuru.SEBZE },
                new Malzeme { Isim = "�arliston Biber", Tur = MalzemeTuru.SEBZE },
                new Malzeme { Isim = "Yumurta", Tur = MalzemeTuru.DIGER },
                new Malzeme { Isim = "Sucuk", Tur = MalzemeTuru.KIRMIZI_ET }
                );
            context.SaveChanges();

            context.Icerikler.AddOrUpdate(
                (i => new { i.YemekID, i.MalzemeID }),
                new Icerik { YemekID = 1, MalzemeID = 1, MiktarAdeti = 2, MiktarTuru = MiktarTuru.SU_BARDAGI },
                new Icerik { YemekID = 1, MalzemeID = 2, MiktarAdeti = 1, MiktarTuru = MiktarTuru.CAY_BARDAGI },
                new Icerik { YemekID = 1, MalzemeID = 3, MiktarAdeti = 1, MiktarTuru = MiktarTuru.YEMEK_KASIGI },
                new Icerik { YemekID = 1, MalzemeID = 4, MiktarAdeti = 2, MiktarTuru = MiktarTuru.CAY_KASIGI },
                new Icerik { YemekID = 1, MalzemeID = 5, MiktarAdeti = 4, MiktarTuru = MiktarTuru.SU_BARDAGI },
                new Icerik { YemekID = 2, MalzemeID = 6, MiktarAdeti = 2, MiktarTuru = MiktarTuru.SU_BARDAGI },
                new Icerik { YemekID = 2, MalzemeID = 5, MiktarAdeti = 10, MiktarTuru = MiktarTuru.SU_BARDAGI },
                new Icerik { YemekID = 2, MalzemeID = 7, MiktarAdeti = 1, MiktarTuru = MiktarTuru.YEMEK_KASIGI },
                new Icerik { YemekID = 2, MalzemeID = 8, MiktarAdeti = 1, MiktarTuru = MiktarTuru.YEMEK_KASIGI },
                new Icerik { YemekID = 2, MalzemeID = 9, MiktarAdeti = 2, MiktarTuru = MiktarTuru.CAY_KASIGI },
                new Icerik { YemekID = 3, MalzemeID = 10, MiktarAdeti = 1, MiktarTuru = MiktarTuru.ADET },
                new Icerik { YemekID = 3, MalzemeID = 5, MiktarAdeti = 4, MiktarTuru = MiktarTuru.SU_BARDAGI },
                new Icerik { YemekID = 4, MalzemeID = 11, MiktarAdeti = 4, MiktarTuru = MiktarTuru.ADET },
                new Icerik { YemekID = 4, MalzemeID = 5, MiktarAdeti = 1, MiktarTuru = MiktarTuru.SU_BARDAGI },
                new Icerik { YemekID = 4, MalzemeID = 12, MiktarAdeti = 250, MiktarTuru = MiktarTuru.GRAM },
                new Icerik { YemekID = 5, MalzemeID = 15, MiktarAdeti = 2, MiktarTuru = MiktarTuru.ADET }
                );
            context.SaveChanges();
        }
    }
}
