﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YemekSitesi.Models
{
    public enum MiktarTuru
    {
        SU_BARDAGI, CAY_KASIGI, CAY_BARDAGI, YEMEK_KASIGI, GRAM, LITRE, TATLI_KASIGI, ADET  
    }

    public class Icerik
    {
        public int IcerikID { get; set; }
        public int YemekID { get; set; }
        public int MalzemeID { get; set; }
        public int MiktarAdeti { get; set; }
        public MiktarTuru MiktarTuru { get; set; }

        public virtual Yemek Yemek { get; set; }
        public virtual Malzeme Malzeme { get; set; }
        
    }
}