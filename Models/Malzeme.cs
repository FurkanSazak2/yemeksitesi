﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YemekSitesi.Models
{
    public class Malzeme
    {
        public int ID { get; set; }
        public string Isim { get; set; }
        public string Tur { get; set; }
    }

    public static class MalzemeTuru
    {
        public static string SEBZE = "Sebze";
        public static string BAHARAT = "Baharat";
        public static string KIRMIZI_ET = "Kırmızı Et";
        public static string BEYAZ_ET = "Beyaz Et";
        public static string BALIK = "Balık";
        public static string BAKLAGIL = "Baklagil";
        public static string DIGER = "Diğer";
    }
}