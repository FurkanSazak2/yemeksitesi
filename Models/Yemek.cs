﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSitesi.Models
{
    public class Yemek
    {
        public int ID { get; set; }
        [Required]
        public string Isim { get; set;}
        public string Mensei { get; set; }
        public int Zorluk { get; set; }
        public int Kisi { get; set; }
        public int PisirmeSuresi { get; set; }
        public String Tur { get; set; }
        [Required]
        public string Hazirlanis { get; set; }

        public virtual ICollection<Icerik> Icerikler { get; set; }
    }

    public static class YemekTuru
    {
        public static string CORBALAR = "ÇORBALAR";
        public static string TATLILAR = "TATLILAR";
        public static string ET_YEMEKLERI = "ET-BALIK-TAVUK";
        public static string KAHVALTILIKLAR = "KAHVALTILIKLAR";
        public static string SEBZELI_YEMEKLER = "SEBZELİ YEMEKLER";
        public static string HAMUR_ISLERI = "HAMUR İŞLERİ";
        public static string PILAVLAR = "PİLAVLAR";
    }
}